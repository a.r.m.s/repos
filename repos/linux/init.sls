{% from "repos/linux/os-map.jinja" import repos with context %}

repo-cleanse:
    file.directory:
    {% if grains['os_family'] == 'Suse' %}
        - name: /etc/zypp/repos.d/
    {% elif grains['os_family'] == 'RedHat' %}
        - name: /etc/yum.repos.d/
    {% endif %}
        - clean: True

add-repos:
    file.managed:
      - name: {{ repos.repo_location }}
      - source: {{ repos.repo_source }}
      - skip_verify: true
